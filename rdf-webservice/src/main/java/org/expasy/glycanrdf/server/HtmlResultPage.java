package org.expasy.glycanrdf.server;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public class HtmlResultPage {

    private final StringBuilder stringBuilder = new StringBuilder();

    public void addHeader(String size){
        String header = "<!DOCTYPE html>\n" +
                "<html xmlns=\"http://www.w3.org/1999/html\">\n" +
                "<head>\n" +
                "    <title>Substructure Search</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                "</head>\n" +
                "<body>\n" +
                "<br />\n" +
                "<h2>The query has produced "+size+" results on 17000 structures in the Database.</h2>"+
                "<table style=\"width:100%\">";
        stringBuilder.append(header);
    }


    public void addResult(String id){
        String result = "<tr>\n" +
                        "    <td> Glycome Db ID of the Sugar " + id +"</td>\n" +
                        "    <td> <a href=\"http://www.glycome-db.org/database/showStructure.action?glycomeId="+id+"\"><img src=\"http://www.glycome-db.org/getSugarImage.action?id="+id+"&type=cfg\"></a></td>"+
                        "</tr>";
        stringBuilder.append(result);
    }

    public void addFooter(){
        String footer = "</table>\n" +
                        "</body>\n" +
                        "</html>";

        stringBuilder.append(footer);
    }


    public String build(){
        return stringBuilder.toString();
    }

}
