package org.expasy.glycanrdf;

import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.expasy.glycanrdf.rdf.model.RDFModelOne;
import org.expasy.glycanrdf.reader.CsvFileReader;
import org.expasy.glycanrdf.reader.FileAcceptor;
import org.expasy.glycanrdf.reader.TxtFileReader;
import org.expasy.mzjava.glycomics.io.mol.glycoct.DefaultGlycoCtResolver;
import org.expasy.mzjava.glycomics.io.mol.glycoct.GlycoCTReader;
import org.expasy.mzjava.glycomics.mol.Glycan;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.StringWriter;
import java.util.Map;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class ImportTest {

    @Test  @Ignore
    public void importUnicarbDB() throws Exception {

        System.setProperty(DefaultGlycoCtResolver.PROPERTY_GLYCOCT_MONOSACCHARIDES,getClass().getResource("monosaccharideGlycoCtData.json").getPath());

        CsvFileReader readerCSV = new CsvFileReader();

        Map<String,String> structuresMap = readerCSV.readCsv( "P:\\GGE_Project", "UniCarbDB","UC");

        Model model = ModelFactory.createDefaultModel();

        RDFModelOne rdfModelOne = new RDFModelOne(model);

        int errors = 0;
        for(Map.Entry<String,String> entry : structuresMap.entrySet()){
            GlycoCTReader reader = new GlycoCTReader();

            try {
                Glycan glycan = reader.read(entry.getValue(),entry.getKey());
                rdfModelOne.addGlycanToRDFDataFile(glycan);
            }
            catch (Exception ex){

                System.err.println(entry.getKey() + ";" + ex.getMessage());
                errors++;
            }

        }
        System.out.println("Import : "+ (structuresMap.size() - errors)+ "/"+ structuresMap.size());
        StringWriter writer = new StringWriter();
        RDFDataMgr.write(writer, model, RDFFormat.TTL);

        Assert.assertEquals(
                "<rdf:RDF\n" +
                        "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" > \n" +
                        "</rdf:RDF>\n",
                writer.toString()
        );



    }



    @Test  @Ignore
    public void importGlycomeDB() throws Exception {
        System.setProperty(DefaultGlycoCtResolver.PROPERTY_GLYCOCT_MONOSACCHARIDES,getClass().getResource("monosaccharideGlycoCtData.json").getPath());
        final Model model = ModelFactory.createDefaultModel();
        String dbAcronym = "GC";
        String folder = "P:\\glycomedb\\GlycoCT_condenced\\"; //Change folder According your computer
        TxtFileReader txtReader = new TxtFileReader();

        FileAcceptor acceptor = new FileAcceptor() {
            private final RDFModelOne rdfModelOne = new RDFModelOne(model);
            private final GlycoCTReader importer = new GlycoCTReader();
            private int errors = 0;

            @Override
            public void accept(String fileName, String fileContent) {
                try {
                    Glycan glycan = importer.read(fileContent,fileName);
                    rdfModelOne.addGlycanToRDFDataFile(glycan);
                } catch (Exception e) {
                    //System.err.println(fileName + ";" + e.getMessage());
                    //System.err.println(fileName + ";" );
                    errors++;
                }
            }

            public int getError(){
                return errors;
            }
        };

        txtReader.readTxtOnebyOne(folder,dbAcronym,acceptor);
        System.out.println("Errors: "+acceptor.getError());
        //FileOutputStream stream = new FileOutputStream(new File("P:\\paperRDF\\glycomeDBpaper.ttl"));
        //RDFDataMgr.write(stream, model, RDFFormat.TTL);

        Query query = QueryFactory.create("",Syntax.syntaxARQ);
        QueryExecution qexec = QueryExecutionFactory.create(query, model);


        long start = System.currentTimeMillis();
        //ResultSet resultSet = exec.execSelect();
        ResultSet resultSet = qexec.execSelect();
        long stop = System.currentTimeMillis();
        System.out.println(resultSet.getRowNumber());
        System.out.println("Time : " + ((stop - start) / 1000.0) + "[s]");
        Thread.sleep(2000);

    }

    @Test  @Ignore
    public void getstructureId() throws Exception {
        System.setProperty(DefaultGlycoCtResolver.PROPERTY_GLYCOCT_MONOSACCHARIDES,getClass().getResource("monosaccharideGlycoCtData.json").getPath());
        String dbAcronym = "GC";
        String folder = "P:\\glycomedb\\GlycoCT_condenced\\"; //Change folder According your computer
        TxtFileReader txtReader = new TxtFileReader();

        FileAcceptor acceptor = new FileAcceptor() {
            private final GlycoCTReader importer = new GlycoCTReader();
            private int errors = 0;

            @Override
            public void accept(String fileName, String fileContent) {
                try {
                    Glycan glycan = importer.read(fileContent,fileName);
                    if(glycan.size() > 25 ){
                        System.out.println(glycan.getDatabaseIdentifier() +"|"+ glycan.size());
                    }
                } catch (Exception e) {
                    errors++;
                }
            }

            public int getError(){
                return errors;
            }
        };

        txtReader.readTxtOnebyOne(folder,dbAcronym,acceptor);
        System.out.println("Errors: "+acceptor.getError());
    }
}