package org.expasy.glycanrdf.reader;


import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.Map;

/**
 * @author jmarieth
 * @version sqrt -1.
 */

public class CsvFileReaderTest {

    @Test
    public void testGetReadCsv() throws Exception {

        String dbAcronym = "UC";
        CsvFileReader csv = new CsvFileReader();

        String fileName = "UniCarbDB";
        String fileExt = "csv";

        URL fileUrl = getClass().getResource( fileName +"."+ fileExt);
        File file = new File(fileUrl.toURI());

        Map<String,String> sequences = csv.getCsvData(fileName, "csv", dbAcronym);//csv.readCsv(file.getParent(), fileName, dbAcronym);


        String id = "50";
        String uc_50 = "RES\n" +
                "1b:b-dglc-HEX-1:5\n" +
                "2s:n-acetyl\n" +
                "3b:a-lgal-HEX-1:5|6:d\n" +
                "4b:b-dgal-HEX-1:5\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(3+1)3d\n" +
                "3:1o(4+1)4d" ;

        String testKey = csv.composeKey(dbAcronym, id);

        Assert.assertEquals(sequences.get(testKey), uc_50);
    }


    @Test
    public void testComposeKey() throws Exception {
        CsvFileReader csv = new CsvFileReader();
        String dbAcronym = "UC";
        String id = "50";

        Assert.assertEquals(csv.composeKey(dbAcronym, id), "UC_50");
    }



}
