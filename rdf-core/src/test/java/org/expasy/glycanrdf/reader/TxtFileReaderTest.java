package org.expasy.glycanrdf.reader;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

import java.io.File;
import java.io.FileWriter;
import java.util.Map;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class TxtFileReaderTest  {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testReadAllTxtFromFolder() throws Exception {

        folder.create();

        String contentTp1 = "CiaoCiaoCiao";
        String contentTp2 = "SalutSalutSalut";

        File tempfile = folder.newFile("test1.json");
        File tempfile2 = folder.newFile("test2.json");
        FileWriter writer = new FileWriter(tempfile);
        writer.write(contentTp1);
        writer.close();
        FileWriter writer2 = new FileWriter(tempfile2);
        writer2.write(contentTp2);
        writer2.close();

        TxtFileReader reader = new TxtFileReader();

        Map<String,String> result = reader.readAllTxtFromFolder(tempfile.getParent(), "TEST");

        Assert.assertTrue(result.containsKey("TEST_test1"));
        Assert.assertTrue(result.containsKey("TEST_test2"));
        Assert.assertEquals(result.get("TEST_test1"), "CiaoCiaoCiao");
        Assert.assertEquals(result.get("TEST_test2"), "SalutSalutSalut");
        folder.delete();
    }

    @Test
    public void testReadTxtOnebyOne() throws Exception {

        TxtFileReader reader = new TxtFileReader();

        folder.create();

        String contentTp1 = "CiaoCiaoCiao";
        String contentTp2 = "SalutSalutSalut";

        File tempfile = folder.newFile("test1.json");
        File tempfile2 = folder.newFile("test2.json");

        FileWriter writer = new FileWriter(tempfile);
        writer.write(contentTp1);
        writer.close();
        FileWriter writer2 = new FileWriter(tempfile2);
        writer2.write(contentTp2);
        writer2.close();

        FileAcceptor acceptor = Mockito.mock(FileAcceptor.class);

        reader.readTxtOnebyOne(tempfile.getParent(),"TEST",acceptor);

        Mockito.verify(acceptor, Mockito.times(1)).accept("TEST_test1","CiaoCiaoCiao");
        Mockito.verify(acceptor, Mockito.times(1)).accept("TEST_test2","SalutSalutSalut");
        Mockito.verifyNoMoreInteractions(acceptor);
    }
}
