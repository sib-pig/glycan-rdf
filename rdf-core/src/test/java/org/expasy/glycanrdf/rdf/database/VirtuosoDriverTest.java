package org.expasy.glycanrdf.rdf.database;

import org.expasy.glycanrdf.configuration.ConfigurationManager;
import org.expasy.glycanrdf.configuration.DefaultConfigurationManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openide.util.Lookup;

import java.io.File;
import java.io.FileWriter;

public class VirtuosoDriverTest{

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();


    @Test
    public void testGetDriverType() throws Exception {

        File temporaryfile = folder.newFile("tempConfigTest.xml");
        FileWriter fileWriter = new FileWriter(temporaryfile);
        fileWriter.write("<config>\n" +
                "    <database>virtuoso</database>\n" +
                "    <virtuoso>\n" +
                "        <url>127.0.0.1</url>\n" +
                "        <port>1521</port>\n" +
                "        <user>sarah</user>\n" +
                "        <password>mnsnmadnasjdbhejkghdasdbhamndbehbdamndbandbevan</password>\n" +
                "        <dbname>mnsnmadnasjdbhejkghdasdbhamndbehbdamndbandbevan</dbname>\n" +
                "    </virtuoso>\n" +
                "</config>");

        fileWriter.close();
        System.setProperty(DefaultConfigurationManager.PROPERTY_CONFIGURATION, temporaryfile.getPath());
        Lookup.getDefault().lookup(ConfigurationManager.class).reload();
        VirtuosoDriver driver = new VirtuosoDriver();
        ConfigurationManager manager = Lookup.getDefault().lookup(ConfigurationManager.class);
        Assert.assertEquals("sarah",manager.getProperty("virtuoso.user"));
        Assert.assertEquals("virtuoso", driver.getDriverType());
        Assert.assertNotEquals("jena",driver.getDriverType());
    }

    @After
    public void after() throws Exception {
        System.clearProperty(DefaultConfigurationManager.PROPERTY_CONFIGURATION);
    }

}