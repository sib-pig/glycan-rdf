package org.expasy.glycanrdf.rdf.database;

import org.expasy.glycanrdf.configuration.ConfigurationManager;
import org.expasy.glycanrdf.configuration.DefaultConfigurationManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openide.util.Lookup;

import java.io.File;
import java.io.FileWriter;

public class DatabaseConnectorTest {



    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testGetDatabase() throws Exception {

        File temporaryfile = folder.newFile("tempConfig.xml");

        FileWriter fileWriter = new FileWriter(temporaryfile);
        fileWriter.write("<config>\n" +
                "    <database>ADataBase</database>\n" +
                "    <virtuoso>\n" +
                "        <url>127.0.0.1</url>\n" +
                "        <port>1521</port>\n" +
                "        <user>djhasdasjhdasdaasdasdasdah</user>\n" +
                "        <password>mnsnmadnasjdbhejkghdasdbhamndbehbdamndbandbevan</password>\n" +
                "        <dbname>mnsnmadnasjdbhejkghdasdbhamndbehbdamndbandbevan</dbname>\n" +
                "    </virtuoso>\n" +
                "</config>");

        fileWriter.close();
        System.setProperty(DefaultConfigurationManager.PROPERTY_CONFIGURATION,temporaryfile.getPath());
        Lookup.getDefault().lookup(ConfigurationManager.class).reload();

        DatabaseConnector connector = new DatabaseConnector();

        Assert.assertEquals("ADataBase",connector.getDatabaseType());
        Assert.assertNotEquals("virtuoso",connector.getDatabaseType());

    }

    @Test (expected = IllegalStateException.class)
    public void testGetConnection() throws  Exception{

        File temporaryfile = folder.newFile("tempConfig.xml");

        FileWriter fileWriter = new FileWriter(temporaryfile);
        fileWriter.write("<config>\n" +
                "    <database>nodatabase</database>\n" +
                "    <virtuoso>\n" +
                "        <url>127.0.0.1</url>\n" +
                "        <port>1521</port>\n" +
                "        <user>admin</user>\n" +
                "        <password>pass</password>\n" +
                "        <dbname>admin</dbname>\n" +
                "    </virtuoso>\n" +
                "</config>");

        fileWriter.close();
        System.setProperty(DefaultConfigurationManager.PROPERTY_CONFIGURATION,temporaryfile.getPath());
        Lookup.getDefault().lookup(ConfigurationManager.class).reload();
        DatabaseConnector connector = new DatabaseConnector();
        connector.getConnection();
    }

    @Test (expected = IllegalStateException.class)
    public void testGetConnection2() throws  Exception{

        File temporaryfile = folder.newFile("tempConfig.xml");

        FileWriter fileWriter = new FileWriter(temporaryfile);
        fileWriter.write("<config>\n" +
                "    <database>nodatabase</database>\n" +
                "    <nodatabase>\n" +
                "        <url>127.0.0.1</url>\n" +
                "        <port>1521</port>\n" +
                "        <login>admin</login>\n" +
                "        <password>pass</password>\n" +
                "    </nodatabase>\n" +
                "</config>");

        fileWriter.close();
        System.setProperty(DefaultConfigurationManager.PROPERTY_CONFIGURATION,temporaryfile.getPath());
        Lookup.getDefault().lookup(ConfigurationManager.class).reload();
        DatabaseConnector connector = new DatabaseConnector();
        connector.getConnection();
    }

    @After
    public void after()  throws  Exception{
        System.clearProperty(DefaultConfigurationManager.PROPERTY_CONFIGURATION);
        folder.delete();
    }
}