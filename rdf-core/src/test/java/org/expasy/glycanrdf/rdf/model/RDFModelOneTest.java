package org.expasy.glycanrdf.rdf.model;

import com.google.common.base.Optional;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFFormat;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.glycomics.mol.*;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openide.util.Lookup;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class    RDFModelOneTest {

    MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);
    SubstituentLookup substituentLookup = Lookup.getDefault().lookup(SubstituentLookup.class);


    @Test
    public void testGlycoCt(){

        Model model = ModelFactory.createDefaultModel();


        String glycoCT =    "RES\n" +
                            "1b:b-dglc-HEX-1:5\n" +
                            "2s:n-acetyl\n" +
                            "3b:b-dgal-HEX-1:5\n" +
                            "LIN\n" +
                            "1:1d(2+1)2n\n" +
                            "2:1o(6+1)3d";


        RDFModelOne rdfDatafile = new RDFModelOne(model);
        rdfDatafile.addGlycoCTToRDFDataFile(glycoCT,"database");
        OutputStream stream = new ByteArrayOutputStream();
        rdfDatafile.write(stream, RDFFormat.TTL);

        Assert.assertTrue(stream.toString().contains("<http://mzjava.expasy.org/glycan/database>\n" +
                "        <http://mzjava.expasy.org/predicate/has_structure>\n" +
                "                <http://mzjava.expasy.org/structureConnection/database> .\n"));


        Assert.assertTrue(stream.toString().contains("<http://mzjava.expasy.org/component/database/2>\n" +
                "        <http://mzjava.expasy.org/predicate/is_a_Gal>\n" +
                "                <http://mzjava.expasy.org/component/database/2> ;\n" +
                "        <http://mzjava.expasy.org/predicate/is_monosaccharide>\n" +
                "                <http://mzjava.expasy.org/component/database/2> .\n"));


        Assert.assertTrue(stream.toString().contains("<http://mzjava.expasy.org/structureConnection/database>\n" +
                        "        <http://mzjava.expasy.org/predicate/has_components>\n" +
                        "                <http://mzjava.expasy.org/component/database/2> , <http://mzjava.expasy.org/component/database/1> , <http://mzjava.expasy.org/component/database/0> .\n"));

        Assert.assertTrue(stream.toString().contains("<http://mzjava.expasy.org/component/database/1>\n" +
                "        <http://mzjava.expasy.org/predicate/is_a_NAcetyl>\n" +
                "                <http://mzjava.expasy.org/component/database/1> ;\n" +
                "        <http://mzjava.expasy.org/predicate/is_substituent>\n" +
                "                <http://mzjava.expasy.org/component/database/1> .\n"));


        Assert.assertTrue(stream.toString().contains(
                "<http://mzjava.expasy.org/component/database/0>\n" +
                "        <http://mzjava.expasy.org/predicate/has_anomerCarbon_1>\n" +
                "                <http://mzjava.expasy.org/component/database/2> ;\n" +
                "        <http://mzjava.expasy.org/predicate/has_anomerConnection_beta>\n" +
                "                <http://mzjava.expasy.org/component/database/2> ;\n" +
                "        <http://mzjava.expasy.org/predicate/has_linkedCarbon_2>\n" +
                "                <http://mzjava.expasy.org/component/database/1> ;\n" +
                "        <http://mzjava.expasy.org/predicate/has_linkedCarbon_6>\n" +
                "                <http://mzjava.expasy.org/component/database/2> ;\n" +
                "        <http://mzjava.expasy.org/predicate/is_GlycosidicLinkage>\n" +
                "                <http://mzjava.expasy.org/component/database/2> ;\n" +
                "        <http://mzjava.expasy.org/predicate/is_SubstituentLinkage>\n" +
                "                <http://mzjava.expasy.org/component/database/1> ;\n" +
                "        <http://mzjava.expasy.org/predicate/is_a_Glc>\n" +
                "                <http://mzjava.expasy.org/component/database/0> ;\n"));


        Assert.assertTrue(stream.toString().contains(
                "        <http://mzjava.expasy.org/predicate/is_connected>\n"));

        Assert.assertTrue(stream.toString().contains("<http://mzjava.expasy.org/component/database/2>"));
        Assert.assertTrue(stream.toString().contains("<http://mzjava.expasy.org/component/database/1>"));

        Assert.assertTrue(stream.toString().contains(
                                "        <http://mzjava.expasy.org/predicate/is_monosaccharide>\n" +
                "                <http://mzjava.expasy.org/component/database/0> ."));


    }

    @Test
    public void testCreateStructureWithNAc() throws Exception {

        Model model = ModelFactory.createDefaultModel();
        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), man, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        builder.add(substituentLookup.getNew("NAcetyl"), glc, new SubstituentLinkage(Optional.<Integer>absent(), Optional.of(Composition.parseComposition("H-1"))));

        Glycan glycan = builder.build();


        RDFModelOne rdfDatafile = new RDFModelOne(model);
        rdfDatafile.addGlycanToRDFDataFile(glycan);
        OutputStream stream = new ByteArrayOutputStream();
        rdfDatafile.write(stream, RDFFormat.TTL);

        Assert.assertEquals(
                "<http://mzjava.expasy.org/component/A/4>\n" +
                        "        <http://mzjava.expasy.org/predicate/is_a_Fuc>\n" +
                        "                <http://mzjava.expasy.org/component/A/4> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_monosaccharide>\n" +
                        "                <http://mzjava.expasy.org/component/A/4> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/component/A/2>\n" +
                        "        <http://mzjava.expasy.org/predicate/is_a_Gal>\n" +
                        "                <http://mzjava.expasy.org/component/A/2> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_monosaccharide>\n" +
                        "                <http://mzjava.expasy.org/component/A/2> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/structureConnection/A>\n" +
                        "        <http://mzjava.expasy.org/predicate/has_components>\n" +
                        "                <http://mzjava.expasy.org/component/A/4> , <http://mzjava.expasy.org/component/A/3> , <http://mzjava.expasy.org/component/A/2> , <http://mzjava.expasy.org/component/A/1> , <http://mzjava.expasy.org/component/A/0> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/component/A/0>\n" +
                        "        <http://mzjava.expasy.org/predicate/is_GlycosidicLinkage>\n" +
                        "                <http://mzjava.expasy.org/component/A/3> , <http://mzjava.expasy.org/component/A/2> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_SubstituentLinkage>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_a_Glc>\n" +
                        "                <http://mzjava.expasy.org/component/A/0> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_connected>\n" +
                        "                <http://mzjava.expasy.org/component/A/3> , <http://mzjava.expasy.org/component/A/2> , <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_monosaccharide>\n" +
                        "                <http://mzjava.expasy.org/component/A/0> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/component/A/3>\n" +
                        "        <http://mzjava.expasy.org/predicate/is_GlycosidicLinkage>\n" +
                        "                <http://mzjava.expasy.org/component/A/4> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_a_Man>\n" +
                        "                <http://mzjava.expasy.org/component/A/3> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_connected>\n" +
                        "                <http://mzjava.expasy.org/component/A/4> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_monosaccharide>\n" +
                        "                <http://mzjava.expasy.org/component/A/3> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/glycan/A>\n" +
                        "        <http://mzjava.expasy.org/predicate/has_structure>\n" +
                        "                <http://mzjava.expasy.org/structureConnection/A> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/component/A/1>\n" +
                        "        <http://mzjava.expasy.org/predicate/is_a_NAcetyl>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_substituent>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> .\n",
                stream.toString()
        );

    }



    @Test
    public void testCreateStructure() throws Exception {

        Model model = ModelFactory.createDefaultModel();
        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        GlycosidicLinkage linkage0 = new GlycosidicLinkage(Optional.of(Anomericity.alpha), Optional.of(1), Optional.of(3), Optional.<Composition>absent(), Optional.<Composition>absent());


        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, linkage0);



        Glycan glycan = builder.build();

        RDFModelOne rdfDatafile = new RDFModelOne(model);
        rdfDatafile.addGlycanToRDFDataFile(glycan);
        OutputStream stream = new ByteArrayOutputStream();
        rdfDatafile.write(stream, RDFFormat.TTL);

        Assert.assertEquals(
                "<http://mzjava.expasy.org/component/A/1>\n" +
                        "        <http://mzjava.expasy.org/predicate/is_a_Gal>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_monosaccharide>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/structureConnection/A>\n" +
                        "        <http://mzjava.expasy.org/predicate/has_components>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> , <http://mzjava.expasy.org/component/A/0> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/component/A/0>\n" +
                        "        <http://mzjava.expasy.org/predicate/has_anomerCarbon_1>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/has_anomerConnection_alpha>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/has_linkedCarbon_3>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_GlycosidicLinkage>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_a_Glc>\n" +
                        "                <http://mzjava.expasy.org/component/A/0> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_connected>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_monosaccharide>\n" +
                        "                <http://mzjava.expasy.org/component/A/0> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/glycan/A>\n" +
                        "        <http://mzjava.expasy.org/predicate/has_structure>\n" +
                        "                <http://mzjava.expasy.org/structureConnection/A> .\n",
                stream.toString());

    }

    @Test
    public void testCreateStructureWithSulfate() throws Exception {

        Model model = ModelFactory.createDefaultModel();
        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        builder.add(substituentLookup.getNew("Sulfate"), glc, new SubstituentLinkage(Optional.<Integer>absent(), Optional.of(Composition.parseComposition("H-1"))));

        Glycan glycan = builder.build();

        RDFModelOne rdfDatafile = new RDFModelOne(model);
        rdfDatafile.addGlycanToRDFDataFile(glycan);
        OutputStream stream = new ByteArrayOutputStream();
        rdfDatafile.write(stream, RDFFormat.TTL);

        Assert.assertEquals(
                "<http://mzjava.expasy.org/component/A/1>\n" +
                        "        <http://mzjava.expasy.org/predicate/is_a_Sulfate>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_substituent>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/structureConnection/A>\n" +
                        "        <http://mzjava.expasy.org/predicate/has_components>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> , <http://mzjava.expasy.org/component/A/0> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/component/A/0>\n" +
                        "        <http://mzjava.expasy.org/predicate/is_SubstituentLinkage>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_a_Glc>\n" +
                        "                <http://mzjava.expasy.org/component/A/0> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_connected>\n" +
                        "                <http://mzjava.expasy.org/component/A/1> ;\n" +
                        "        <http://mzjava.expasy.org/predicate/is_monosaccharide>\n" +
                        "                <http://mzjava.expasy.org/component/A/0> .\n" +
                        "\n" +
                        "<http://mzjava.expasy.org/glycan/A>\n" +
                        "        <http://mzjava.expasy.org/predicate/has_structure>\n" +
                        "                <http://mzjava.expasy.org/structureConnection/A> .\n",
                stream.toString());

    }

    @Test @Ignore //Fixed in standAloneStructure
    public void testCreateStructureLewisUnknowArm() throws Exception {

        Model model = ModelFactory.createDefaultModel();

        String glycoCT = "RES\n" +
                "1b:b-dglc-HEX-1:5\n" +
                "2s:n-acetyl\n" +
                "3b:b-dglc-HEX-1:5\n" +
                "4s:n-acetyl\n" +
                "5b:b-dman-HEX-1:5\n" +
                "6b:a-dman-HEX-1:5\n" +
                "7b:b-dglc-HEX-1:5\n" +
                "8s:n-acetyl\n" +
                "9b:a-dman-HEX-1:5\n" +
                "10b:b-dglc-HEX-1:5\n" +
                "11s:n-acetyl\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(4+1)3d\n" +
                "3:3d(2+1)4n\n" +
                "4:3o(4+1)5d\n" +
                "5:5o(3+1)6d\n" +
                "6:6o(2+1)7d\n" +
                "7:7d(2+1)8n\n" +
                "8:5o(6+1)9d\n" +
                "9:9o(2+1)10d\n" +
                "10:10d(2+1)11n\n" +
                "UND\n" +
                "UND1:100.0:100.0\n" +
                "ParentIDs:1|3|5|6|7|9|10\n" +
                "SubtreeLinkageID1:u(4+1)u\n" +
                "RES\n" +
                "12b:b-dgal-HEX-1:5\n" +
                "13b:b-dglc-HEX-1:5\n" +
                "14b:a-lgal-HEX-1:5|6:d\n" +
                "15b:b-dgal-HEX-1:5\n" +
                "16b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" +
                "17s:n-acetyl\n" +
                "18s:n-acetyl\n" +
                "LIN\n" +
                "11:12o(3+1)13d\n" +
                "12:13o(4+1)14d\n" +
                "13:13o(3|4+1)15d\n" +
                "14:15o(3+2)16d\n" +
                "15:16d(5+1)17n\n" +
                "16:13d(2+1)18n";

        RDFModelOne rdfDatafile = new RDFModelOne(model);
        rdfDatafile.addGlycoCTToRDFDataFile(glycoCT,"database");
        OutputStream stream = new ByteArrayOutputStream();
        rdfDatafile.write(stream, RDFFormat.TTL);

        System.out.println("test: " + stream);
    }

    @Test
    public void testCreateSialicAcidUnknownLinkage() throws Exception {

        Model model = ModelFactory.createDefaultModel();
        String glycoCT = "RES\n" +
                "1b:b-dglc-HEX-1:5\n" +
                "2s:n-acetyl\n" +
                "3b:b-dglc-HEX-1:5\n" +
                "4s:n-acetyl\n" +
                "5b:b-dman-HEX-1:5\n" +
                "6b:a-dman-HEX-1:5\n" +
                "7b:b-dglc-HEX-1:5\n" +
                "8s:n-acetyl\n" +
                "9b:b-dgal-HEX-1:5\n" +
                "10b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" +
                "11s:n-acetyl\n" +
                "12b:b-dglc-HEX-1:5\n" +
                "13s:n-acetyl\n" +
                "14b:a-dman-HEX-1:5\n" +
                "15b:b-dglc-HEX-1:5\n" +
                "16s:n-acetyl\n" +
                "17b:b-dgal-HEX-1:5\n" +
                "18b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" +
                "19s:n-acetyl\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(4+1)3d\n" +
                "3:3d(2+1)4n\n" +
                "4:3o(4+1)5d\n" +
                "5:5o(3+1)6d\n" +
                "6:6o(2+1)7d\n" +
                "7:7d(2+1)8n\n" +
                "8:7o(4+1)9d\n" +
                "9:9o(6+2)10d\n" +
                "10:10d(5+1)11n\n" +
                "11:5o(4+1)12d\n" +
                "12:12d(2+1)13n\n" +
                "13:5o(6+1)14d\n" +
                "14:14o(2+1)15d\n" +
                "15:15d(2+1)16n\n" +
                "16:15o(4+1)17d\n" +
                "17:17o(3|6+2)18d\n" +
                "18:18d(5+1)19n";

        RDFModelOne rdfDatafile = new RDFModelOne(model);
        rdfDatafile.addGlycoCTToRDFDataFile(glycoCT, "database");
        OutputStream stream = new ByteArrayOutputStream();
        rdfDatafile.write(stream, RDFFormat.TTL);

        System.out.println("test: " + stream);
    }

    @Test @Ignore //Fixed in standAloneStructure
    public void testCreatePositionFourthBranch() throws Exception {

        Model model = ModelFactory.createDefaultModel();
        String glycoCT = "RES\n" +
                "1b:b-dglc-HEX-1:5\n" +
                "2s:n-acetyl\n" +
                "3b:b-dglc-HEX-1:5\n" +
                "4s:n-acetyl\n" +
                "5b:b-dman-HEX-1:5\n" +
                "6b:a-dman-HEX-1:5\n" +
                "7b:b-dglc-HEX-1:5\n" +
                "8s:n-acetyl\n" +
                "9b:b-dglc-HEX-1:5\n" +
                "10s:n-acetyl\n" +
                "11b:a-dman-HEX-1:5\n" +
                "12b:b-dglc-HEX-1:5\n" +
                "13s:n-acetyl\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(4+1)3d\n" +
                "3:3d(2+1)4n\n" +
                "4:3o(4+1)5d\n" +
                "5:5o(3+1)6d\n" +
                "6:6o(2+1)7d\n" +
                "7:7d(2+1)8n\n" +
                "8:5o(4+1)9d\n" +
                "9:9d(2+1)10n\n" +
                "10:5o(6+1)11d\n" +
                "11:11o(2+1)12d\n" +
                "12:12d(2+1)13n\n" +
                "UND\n" +
                "UND1:100.0:100.0\n" +
                "ParentIDs:1|3|5|6|7|9|11|12\n" +
                "SubtreeLinkageID1:u(4|6+1)u\n" +
                "RES\n" +
                "14b:b-dglc-HEX-1:5\n" +
                "15s:n-acetyl\n" +
                "LIN\n" +
                "13:14d(2+1)15n";

        RDFModelOne rdfDatafile = new RDFModelOne(model);
        rdfDatafile.addGlycoCTToRDFDataFile(glycoCT, "database");
        OutputStream stream = new ByteArrayOutputStream();
        rdfDatafile.write(stream, RDFFormat.TTL);

        System.out.println("test: " + stream);
    }

    @Test @Ignore //Fixed in standAloneStructure
    public void testCreateUnknowBranchingTwoResidues() throws Exception {

        Model model = ModelFactory.createDefaultModel();
        String glycoCT = "RES\n" +
                "1b:b-dglc-HEX-1:5\n" +
                "2s:n-acetyl\n" +
                "3b:b-dglc-HEX-1:5\n" +
                "4s:n-acetyl\n" +
                "5b:b-dman-HEX-1:5\n" +
                "6b:a-dman-HEX-1:5\n" +
                "7b:b-dglc-HEX-1:5\n" +
                "8s:n-acetyl\n" +
                "9b:b-dgal-HEX-1:5\n" +
                "10b:a-dman-HEX-1:5\n" +
                "11b:b-dglc-HEX-1:5\n" +
                "12s:n-acetyl\n" +
                "13b:b-dgal-HEX-1:5\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(4+1)3d\n" +
                "3:3d(2+1)4n\n" +
                "4:3o(4+1)5d\n" +
                "5:5o(3+1)6d\n" +
                "6:6o(2+1)7d\n" +
                "7:7d(2+1)8n\n" +
                "8:7o(4+1)9d\n" +
                "9:5o(6+1)10d\n" +
                "10:10o(2+1)11d\n" +
                "11:11d(2+1)12n\n" +
                "12:11o(4+1)13d\n" +
                "UND\n" +
                "UND1:100.0:100.0\n" +
                "ParentIDs:1|3|5|6|7|9|10|11|13\n" +
                "SubtreeLinkageID1:u(4|6+1)u\n" +
                "RES\n" +
                "14b:b-dglc-HEX-1:5\n" +
                "15s:n-acetyl\n" +
                "LIN\n" +
                "13:14d(2+1)15n\n" +
                "UND2:100.0:100.0\n" +
                "ParentIDs:1|3|5|6|7|9|10|11|13\n" +
                "SubtreeLinkageID1:u(4|6+1)u\n" +
                "RES\n" +
                "16b:b-dglc-HEX-1:5\n" +
                "17b:x-dgal-HEX-1:5\n" +
                "18s:n-acetyl\n" +
                "LIN\n" +
                "14:16o(-1+1)17d\n" +
                "15:16d(2+1)18n";

        RDFModelOne rdfDatafile = new RDFModelOne(model);
        rdfDatafile.addGlycoCTToRDFDataFile(glycoCT, "database");
        OutputStream stream = new ByteArrayOutputStream();
        rdfDatafile.write(stream, RDFFormat.TTL);

        System.out.println("test: " + stream);
    }

    @Test
    public void testCreateDoubleUnknowSlewisOnBiAnt() throws Exception {

        Model model = ModelFactory.createDefaultModel();
        String glycoCT = "RES\n" +
                "1b:b-dglc-HEX-1:5\n" +
                "2s:n-acetyl\n" +
                "3b:b-dglc-HEX-1:5\n" +
                "4s:n-acetyl\n" +
                "5b:b-dman-HEX-1:5\n" +
                "6b:a-dman-HEX-1:5\n" +
                "7b:b-dglc-HEX-1:5\n" +
                "8s:n-acetyl\n" +
                "9b:b-dgal-HEX-1:5\n" +
                "10b:b-dglc-HEX-1:5\n" +
                "11b:a-lgal-HEX-1:5|6:d\n" +
                "12b:b-dgal-HEX-1:5\n" +
                "13b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" +
                "14s:n-acetyl\n" +
                "15s:n-acetyl\n" +
                "16b:a-dman-HEX-1:5\n" +
                "17b:b-dglc-HEX-1:5\n" +
                "18s:n-acetyl\n" +
                "19b:b-dgal-HEX-1:5\n" +
                "20b:b-dglc-HEX-1:5\n" +
                "21b:a-lgal-HEX-1:5|6:d\n" +
                "22b:b-dgal-HEX-1:5\n" +
                "23b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" +
                "24s:n-acetyl\n" +
                "25s:n-acetyl\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(4+1)3d\n" +
                "3:3d(2+1)4n\n" +
                "4:3o(4+1)5d\n" +
                "5:5o(3+1)6d\n" +
                "6:6o(2+1)7d\n" +
                "7:7d(2+1)8n\n" +
                "8:7o(4+1)9d\n" +
                "9:9o(4+1)10d\n" +
                "10:10o(4+1)11d\n" +
                "11:10o(3|4+1)12d\n" +
                "12:12o(3+2)13d\n" +
                "13:13d(5+1)14n\n" +
                "14:10d(2+1)15n\n" +
                "15:5o(6+1)16d\n" +
                "16:16o(2+1)17d\n" +
                "17:17d(2+1)18n\n" +
                "18:17o(4+1)19d\n" +
                "19:19o(4+1)20d\n" +
                "20:20o(4+1)21d\n" +
                "21:20o(3|4+1)22d\n" +
                "22:22o(3+2)23d\n" +
                "23:23d(5+1)24n\n" +
                "24:20d(2+1)25n\n";

        RDFModelOne rdfDatafile = new RDFModelOne(model);
        rdfDatafile.addGlycoCTToRDFDataFile(glycoCT, "database");
        OutputStream stream = new ByteArrayOutputStream();
        rdfDatafile.write(stream, RDFFormat.TTL);

        System.out.println("test: " + stream);
    }
}
