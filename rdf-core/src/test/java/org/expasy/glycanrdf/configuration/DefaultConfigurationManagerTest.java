package org.expasy.glycanrdf.configuration;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class DefaultConfigurationManagerTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testGetPropertyWithExternalFiles() throws Exception {

        File tempfile = folder.newFile("tempConfig.xml");

        FileWriter fileWriter = new FileWriter(tempfile);
        fileWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<config>\n" +
                "    <database>\n" +
                "        <url>127.0.0.1</url>\n" +
                "        <port>1521</port>\n" +
                "        <login>admin</login>\n" +
                "        <password>pass</password>\n" +
                "    </database>\n" +
                "</config>");

        fileWriter.close();
        System.setProperty(DefaultConfigurationManager.PROPERTY_CONFIGURATION, tempfile.getPath());
        ConfigurationManager configurationManager = new DefaultConfigurationManager();
        Assert.assertEquals("127.0.0.1",configurationManager.getProperty("database.url"));
        Assert.assertEquals("1521",configurationManager.getProperty("database.port"));
        Assert.assertEquals("admin",configurationManager.getProperty("database.login"));
        Assert.assertEquals("pass",configurationManager.getProperty("database.password"));
        Assert.assertNotEquals("error", configurationManager.getProperty("database.password"));
        System.clearProperty(DefaultConfigurationManager.PROPERTY_CONFIGURATION);
        tempfile.delete();

    }

    @Test
    public void testGetPropertyWithChangedFile() throws Exception {

        File tempfile = folder.newFile("tempConfig2.xml");

        FileWriter fileWriter = new FileWriter(tempfile);
        fileWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<config>\n" +
                "    <database>\n" +
                "        <url>127.0.0.1</url>\n" +
                "        <port>1521</port>\n" +
                "        <login>admin</login>\n" +
                "        <password>pass</password>\n" +
                "    </database>\n" +
                "</config>");
        fileWriter.close();
        System.setProperty(DefaultConfigurationManager.PROPERTY_CONFIGURATION, tempfile.getPath());
        ConfigurationManager configurationManager = new DefaultConfigurationManager();

        Assert.assertEquals("127.0.0.1",configurationManager.getProperty("database.url"));
        Assert.assertEquals("1521",configurationManager.getProperty("database.port"));
        Assert.assertEquals("admin",configurationManager.getProperty("database.login"));
        Assert.assertEquals("pass",configurationManager.getProperty("database.password"));
        Assert.assertNotEquals("error", configurationManager.getProperty("database.password"));

        Thread.sleep(30000);

        FileWriter fileWriter2 = new FileWriter(tempfile);
        fileWriter2.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<config>\n" +
                "    <database>\n" +
                "        <url>127.0.0.1</url>\n" +
                "        <port>152133</port>\n" +
                "        <login>ad</login>\n" +
                "        <password>p</password>\n" +
                "    </database>\n" +
                "</config>");

        fileWriter2.close();

        Assert.assertEquals("127.0.0.1",configurationManager.getProperty("database.url"));
        Assert.assertEquals("152133",configurationManager.getProperty("database.port"));
        Assert.assertEquals("ad",configurationManager.getProperty("database.login"));
        Assert.assertEquals("p",configurationManager.getProperty("database.password"));
        Assert.assertNotEquals("error", configurationManager.getProperty("database.password"));
        System.clearProperty(DefaultConfigurationManager.PROPERTY_CONFIGURATION);
        configurationManager = null;
        folder.delete();
    }



    @Test
    public void testRealFile() throws Exception {

       DefaultConfigurationManager manager = new DefaultConfigurationManager();
       manager.getProperty("database");
    }

}