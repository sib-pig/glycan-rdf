package org.expasy.glycanrdf.tripleStore;

import com.google.common.base.Optional;
import org.expasy.glycanrdf.query.QueryGenerator;
import org.expasy.glycanrdf.rdf.query.VirtuosoQueryGeneratorExtend;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.glycomics.mol.*;
import org.junit.Assert;
import org.junit.Test;
import org.openide.util.Lookup;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class VirtuosoQueryGeneratorExtendTest {

    MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);
    SubstituentLookup substituentLookup = Lookup.getDefault().lookup(SubstituentLookup.class);

    @Test
    public void testGenerateQueryString() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Glycan glycan = builder.build();

        QueryGenerator queryGenerator = new VirtuosoQueryGeneratorExtend();

        String query = queryGenerator.generateQueryString(glycan);

        String lineSeparator = System.getProperty("line.separator");

        Assert.assertEquals("prefix glycan:  <http://mzjava.expasy.org/glycan/>" + lineSeparator +
                "prefix predicate:  <http://mzjava.expasy.org/predicate/>" + lineSeparator +
                "prefix component:  <http://mzjava.expasy.org/component/>" + lineSeparator +
                "prefix structureConnection:  <http://mzjava.expasy.org/structureConnection/>" + lineSeparator +
                "SELECT DISTINCT ?structureConnection" + lineSeparator +
                "WHERE { " + lineSeparator +
                "?structureConnection predicate:has_components ?component0 . " + lineSeparator +
                "?component0 predicate:is_a_Glc ?component0 . " + lineSeparator +
                "}"+ lineSeparator, query);
    }


    @Test
    public void testGenerateQueryString2() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), man, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        builder.add(substituentLookup.getNew("NAcetyl"), glc, new SubstituentLinkage(Optional.<Integer>absent(), Optional.of(Composition.parseComposition("H-1"))));

        Glycan glycan = builder.build();
        QueryGenerator queryGenerator = new VirtuosoQueryGeneratorExtend();
        String query = queryGenerator.generateQueryString(glycan);
        String lineSeparator = System.getProperty("line.separator");

        Assert.assertEquals("prefix glycan:  <http://mzjava.expasy.org/glycan/>" + lineSeparator +
                "prefix predicate:  <http://mzjava.expasy.org/predicate/>" + lineSeparator +
                "prefix component:  <http://mzjava.expasy.org/component/>" + lineSeparator +
                "prefix structureConnection:  <http://mzjava.expasy.org/structureConnection/>"  + lineSeparator +
                "SELECT DISTINCT ?structureConnection"  + lineSeparator +
                "WHERE { "  + lineSeparator +
                "?structureConnection predicate:has_components ?component0 . "  + lineSeparator +
                "?component0 predicate:is_a_Glc ?component0 . "  + lineSeparator +
                "?component1 predicate:is_a_NAcetyl ?component1 . " + lineSeparator +
                "?component0 predicate:is_connected ?component1 . "  + lineSeparator +
                "?component0 predicate:is_SubstituentLinkage ?component1 . "  + lineSeparator +
                "?component2 predicate:is_a_Gal ?component2 . "  + lineSeparator +
                "?component0 predicate:is_connected ?component2 . "  + lineSeparator +
                "?component0 predicate:is_GlycosidicLinkage ?component2 . "  + lineSeparator +
                "?component3 predicate:is_a_Man ?component3 . "  + lineSeparator +
                "?component0 predicate:is_connected ?component3 . "  + lineSeparator +
                "?component0 predicate:is_GlycosidicLinkage ?component3 . "  + lineSeparator +
                "?component4 predicate:is_a_Fuc ?component4 . "  + lineSeparator +
                "?component3 predicate:is_connected ?component4 . "  + lineSeparator +
                "?component3 predicate:is_GlycosidicLinkage ?component4 . "  + lineSeparator +
                "}"+ lineSeparator ,query);
    }


    @Test
    public void testGenerateQueryString3() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Anomericity.alpha, 1, 3,Composition.parseComposition("OH") , Composition.parseComposition("H")));
        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), glc, new GlycosidicLinkage(Anomericity.beta, 1, 6,Composition.parseComposition("OH") , Composition.parseComposition("H")));
        builder.add(substituentLookup.getNew("NAcetyl"), man, new SubstituentLinkage(2, Composition.parseComposition("OH")));
        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), man, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        builder.add(substituentLookup.getNew("NAcetyl"), glc, new SubstituentLinkage(2, Composition.parseComposition("OH")));

        Glycan glycan = builder.build();
        QueryGenerator queryGenerator = new VirtuosoQueryGeneratorExtend();
        String lineSeparator = System.getProperty("line.separator");
        String query = queryGenerator.generateQueryString(glycan);
        Assert.assertEquals("prefix glycan:  <http://mzjava.expasy.org/glycan/>" + lineSeparator +
                "prefix predicate:  <http://mzjava.expasy.org/predicate/>" + lineSeparator +
                "prefix component:  <http://mzjava.expasy.org/component/>" + lineSeparator +
                "prefix structureConnection:  <http://mzjava.expasy.org/structureConnection/>" + lineSeparator +
                "SELECT DISTINCT ?structureConnection" + lineSeparator +
                "WHERE { " + lineSeparator +
                "?structureConnection predicate:has_components ?component0 . " + lineSeparator +
                "?component0 predicate:is_a_Glc ?component0 . " + lineSeparator +
                "?component1 predicate:is_a_NAcetyl ?component1 . " + lineSeparator +
                "?component0 predicate:is_connected ?component1 . " + lineSeparator +
                "?component0 predicate:is_SubstituentLinkage ?component1 . " + lineSeparator +
                "?component0 predicate:has_linkedCarbon_2 ?component1 . " + lineSeparator +
                "?component2 predicate:is_a_Gal ?component2 . " + lineSeparator +
                "?component0 predicate:is_connected ?component2 . " + lineSeparator +
                "?component0 predicate:is_GlycosidicLinkage ?component2 . " + lineSeparator +
                "?component0 predicate:has_anomerConnection_alpha ?component2 . " + lineSeparator +
                "?component0 predicate:has_linkedCarbon_3 ?component2 . " + lineSeparator +
                "?component0 predicate:has_anomerCarbon_1 ?component2 . " + lineSeparator +
                "?component3 predicate:is_a_Man ?component3 . " + lineSeparator +
                "?component0 predicate:is_connected ?component3 . " + lineSeparator +
                "?component0 predicate:is_GlycosidicLinkage ?component3 . " + lineSeparator +
                "?component0 predicate:has_anomerConnection_beta ?component3 . " + lineSeparator +
                "?component0 predicate:has_linkedCarbon_6 ?component3 . " + lineSeparator +
                "?component0 predicate:has_anomerCarbon_1 ?component3 . " + lineSeparator +
                "?component4 predicate:is_a_NAcetyl ?component4 . " + lineSeparator +
                "?component3 predicate:is_connected ?component4 . " + lineSeparator +
                "?component3 predicate:is_SubstituentLinkage ?component4 . " + lineSeparator +
                "?component3 predicate:has_linkedCarbon_2 ?component4 . " + lineSeparator +
                "?component5 predicate:is_a_Fuc ?component5 . " + lineSeparator +
                "?component3 predicate:is_connected ?component5 . " + lineSeparator +
                "?component3 predicate:is_GlycosidicLinkage ?component5 . " + lineSeparator +
                "}" + lineSeparator , query);
    }
}
