package org.expasy.glycanrdf.reader;


import com.google.common.base.Preconditions;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author jmarieth
 * @author dalocci
 * @version sqrt -1.
 */

public class CsvFileReader {

    private static final String SEPARATOR = "separator";
    private static final String QUOTE_CHAR = "quotechar";
    private static final String FILE_EXT = "fileExtension";
    private static final String SUPP_HEADERS = "suppressHeaders";

    public CsvFileReader(String separator, String quotechar, String fileExtension, Boolean suppressHeaders){

        Properties props = new java.util.Properties();
        props.put(SEPARATOR, separator); // separator is a comma
        props.put(SUPP_HEADERS, suppressHeaders.toString()); // first line contains data
        props.put(FILE_EXT, fileExtension); // file extension is .csv
        props.put(QUOTE_CHAR, quotechar); // quotechar is single quote

    }

    public CsvFileReader(){

        Properties props = new java.util.Properties();
        props.put(SEPARATOR, ","); // separator is a comma
        props.put(SUPP_HEADERS, "true"); // first line contains data
        props.put(FILE_EXT, ".csv"); // file extension is .csv
        props.put(QUOTE_CHAR, "\'"); // quotechar is single quote

    }

    public Map<String,String> readCsv(String fileDir, String fileName, String dbAcronym){

        Preconditions.checkNotNull(fileDir);
        Preconditions.checkNotNull(fileName);
        Preconditions.checkNotNull(dbAcronym);

        Map<String,String> results = new HashMap<String,String>();

        try{

            Class.forName("org.relique.jdbc.csv.CsvDriver");
            getConnection(fileDir,fileName,dbAcronym,results);

        }catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }

        return results;
    }

    private void getConnection(String fileDir, String fileName, String dbAcronym, Map<String,String> results){
        try (Connection connection = DriverManager.getConnection("jdbc:relique:csv:" + fileDir)) {
            getStatement(connection,fileName,dbAcronym,results);
        } catch (SQLException e) {
            throw new IllegalStateException("Cannot create connection. ",e);
        }
    }

    private void getStatement(Connection connection,String fileName, String dbAcronym,  Map<String,String> results){
        try(Statement stmt = connection.createStatement()) {
            getResults(stmt,fileName,dbAcronym,results);
        } catch (SQLException e) {
            throw new IllegalStateException("Cannot create statement. ",e);
        }
    }

    private void getResults(Statement statement, String fileName, String dbAcronym, Map<String,String> results){

        Preconditions.checkNotNull(results);
        Preconditions.checkNotNull(statement);

        String query = "SELECT Glycan_Seq_ID, Glyco_CT FROM " + fileName;

        try(ResultSet resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                String key = composeKey(dbAcronym, resultSet.getString("Glycan_Seq_ID"));
                String value = resultSet.getString("Glyco_CT");
                results.put(key, value);
            }
        } catch (SQLException e) {
            throw new IllegalStateException("Cannot execute query. ",e);
        }

    }

    public String composeKey (String dbAcronym, String id){
        Preconditions.checkNotNull(dbAcronym);
        Preconditions.checkNotNull(id);

        return dbAcronym + "_" + id;
    }

    public Map<String,String> getCsvData(String fileName, String fileExt, String dbAcronym){

        URL fileUrl = getClass().getResource("./" + fileName +"."+ fileExt);
        File file = null;

        try{
            file = new File(fileUrl.toURI());

        } catch (URISyntaxException e) {
            throw new IllegalStateException("Cannot open the file",e);
        }

        return readCsv(file.getParent(), fileName, dbAcronym);

    }

}