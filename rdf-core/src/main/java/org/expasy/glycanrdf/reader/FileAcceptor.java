package org.expasy.glycanrdf.reader;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public interface FileAcceptor {

    void accept (String fileName, String filecontent);

    int getError();
}
