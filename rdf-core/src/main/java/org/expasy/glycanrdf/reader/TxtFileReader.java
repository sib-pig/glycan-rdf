package org.expasy.glycanrdf.reader;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author dalocci
 * @version sqrt -1.
 */
public class TxtFileReader {

    public Map<String,String> readAllTxtFromFolder(String fileDir, String dbAcronym){

        Preconditions.checkNotNull(fileDir);
        Preconditions.checkNotNull(dbAcronym);

        Map<String,String> resultMap = new HashMap<String, String>();

        File folder = new File(fileDir);

        File[] files = folder.listFiles();
        if(files == null) files = new File[0];
        for (File fileEntry : files) {
            if (!fileEntry.isDirectory()) {

                try {
                    int pos = fileEntry.getName().lastIndexOf(".");
                    resultMap.put(dbAcronym + "_" + fileEntry.getName().substring(0,pos), Files.toString(fileEntry, StandardCharsets.UTF_8).trim());
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
        return resultMap;
    }


    public void readTxtOnebyOne(String fileDir, String dbAcronym, FileAcceptor acceptor){
        Preconditions.checkNotNull(fileDir);
        Preconditions.checkNotNull(dbAcronym);
        Preconditions.checkNotNull(acceptor);

        File folder = new File(fileDir);

        File[] files = folder.listFiles();
        if(files == null) files = new File[0];
        for (File fileEntry : files) {
            if (!fileEntry.isDirectory()) {
                try {
                    int pos = fileEntry.getName().lastIndexOf(".");
                    acceptor.accept(dbAcronym + "_" + fileEntry.getName().substring(0, pos), Files.toString(fileEntry, StandardCharsets.UTF_8).trim());
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }
}
