package org.expasy.glycanrdf.configuration;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public interface ConfigurationManager {

    String getProperty(final String key);

    void reload();
}
