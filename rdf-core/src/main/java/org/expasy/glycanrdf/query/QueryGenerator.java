package org.expasy.glycanrdf.query;

import org.expasy.mzjava.glycomics.mol.Glycan;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public interface QueryGenerator {
    String generateQueryString(Glycan glycan);
}
