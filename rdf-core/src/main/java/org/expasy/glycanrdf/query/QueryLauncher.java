package org.expasy.glycanrdf.query;

import com.google.common.base.Preconditions;
import org.apache.log4j.Logger;
import org.expasy.glycanrdf.configuration.ConfigurationManager;
import org.expasy.glycanrdf.neo4j.cypher.CypherQueryService;
import org.expasy.glycanrdf.rdf.database.DatabaseConnector;
import org.expasy.mzjava.glycomics.mol.Glycan;
import org.openide.util.Lookup;
import org.openrdf.query.*;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import java.util.Map;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public class QueryLauncher {

    private static final Logger LOGGER = Logger.getLogger(QueryLauncher.class);
    private final DatabaseConnector connector;

    public QueryLauncher(){
        connector = new DatabaseConnector();
    }

    public void querySPARQL(String query, TupleQueryResultHandler out){

        Preconditions.checkNotNull(query);
        Preconditions.checkNotNull(out);

        RepositoryConnection connection = connector.getConnection();

        TupleQuery tupleQuery;
        try {
            LOGGER.info("Performing query with SPARQL: " + query);
            tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL, query);
            tupleQuery.evaluate(out);
        } catch (RepositoryException | MalformedQueryException | TupleQueryResultHandlerException | QueryEvaluationException e) {
            LOGGER.fatal("Query error SPARQL: " + e.getMessage());
            throw new IllegalStateException("Problem with the repository connection or with the query evaluation.", e);
        } finally {
            closeConnection(connection);
        }
    }

    public String querySPARQLWithStats(String query, TupleQueryResultHandler out){

        Preconditions.checkNotNull(query);
        Preconditions.checkNotNull(out);

        RepositoryConnection connection = connector.getConnection();
        long start = 0;
        long stop = 0;
        TupleQuery tupleQuery;
        try {
            LOGGER.info("Performing query with SPARQL: " + query);
            tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL, query);
            start = System.currentTimeMillis();
            tupleQuery.evaluate(out);
            stop = System.currentTimeMillis();
        } catch (RepositoryException | MalformedQueryException | TupleQueryResultHandlerException | QueryEvaluationException e) {
            LOGGER.fatal("Query error SPARQL: " + e.getMessage());
            throw new IllegalStateException("Problem with the repository connection or with the query evaluation.", e);
        } finally {
            closeConnection(connection);
        }

        return "Time : " + ((stop - start) / 1000.0) + "[s]";
    }

    /**
     * This connection must be close by the user!
     * @return Respository Connection
     */
    public RepositoryConnection getConnection(){
        return connector.getConnection();
    }


    private void closeConnection(RepositoryConnection connection) {
        try {
            connection.close();
        } catch (RepositoryException e) {
            throw new IllegalStateException("Something wrong happens, closing connection to the server.",e);
        }
    }


    public void queryCYPHER(String query, Map<String,String> out ){

        Preconditions.checkNotNull(query);
        Preconditions.checkNotNull(out);
        LOGGER.info("Performing query with neo4j: " + query);
        ConfigurationManager manager = Lookup.getDefault().lookup(ConfigurationManager.class);

        String errorMessage ="There is no configuration for Neo4j. Please add this information to your file" +
                "    <database>neo4j</database>\n" +
                "    <neo4j>\n" +
                "        <path>Neo4j database path (file.db)</path>\n" +
                "    </neo4j>\n";
        if ("neo4j".equals(manager.getProperty("database"))){
            CypherQueryService cypher = new CypherQueryService();
            cypher.getMapResult(cypher.query(query),out);
        } else {
            LOGGER.fatal("Query error neo4j: No configuration found for Neo4j in the configuration file. " + errorMessage);
            throw new IllegalStateException(errorMessage);
        }
    }


    public void queryGlycanSPARQL(Glycan glycan, TupleQueryResultHandler out, QueryGenerator queryGenerator) {

        Preconditions.checkNotNull(glycan);
        Preconditions.checkNotNull(out);

        String query = queryGenerator.generateQueryString(glycan);
        querySPARQL(query, out);
    }
}
