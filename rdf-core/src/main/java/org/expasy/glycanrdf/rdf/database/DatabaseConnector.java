package org.expasy.glycanrdf.rdf.database;

import org.apache.log4j.Logger;
import org.expasy.glycanrdf.configuration.ConfigurationManager;
import org.openide.util.Lookup;
import org.openrdf.repository.RepositoryConnection;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public class DatabaseConnector {

    private static final Logger LOGGER = Logger.getLogger(DatabaseConnector.class);



    public RepositoryConnection getConnection(){

        DatabaseDriver connectionDriver = null;
        for (DatabaseDriver driver : Lookup.getDefault().lookupAll(DatabaseDriver.class)) {
            if(driver.getDriverType().equals(getDatabaseType())){
                connectionDriver = driver;
            }
        }

        if(connectionDriver == null ){
            String errorMessage = "The database specified in the configuration file does not exist. Please read the instruction in the configuration file.";
            LOGGER.fatal(errorMessage);
            throw new IllegalStateException(errorMessage);
        }
        LOGGER.info("Found driver for " + getDatabaseType());
        connectionDriver.initialize();
        LOGGER.info("Driver initialized correctly" + getDatabaseType());

        return connectionDriver.getConnection();
    }




    public String getDatabaseType(){
        ConfigurationManager manager = Lookup.getDefault().lookup(ConfigurationManager.class);
        String databaseType = manager.getProperty("database");
        LOGGER.info("Database in use : " + databaseType);
        return databaseType;
    }
}
