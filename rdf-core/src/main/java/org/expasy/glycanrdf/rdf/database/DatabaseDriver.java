package org.expasy.glycanrdf.rdf.database;

import org.openrdf.repository.RepositoryConnection;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public interface DatabaseDriver {

    RepositoryConnection getConnection();

    String getDriverType();

    void initialize();
}
