package org.expasy.glycanrdf.rdf.model;

import com.google.common.base.Preconditions;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.expasy.mzjava.glycomics.mol.Glycan;

import java.io.OutputStream;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public abstract class RDFDataFileCreator {

    public static final String URL_PREFIX =  "http://mzjava.expasy.org";
    public static final String COMPONENT = "/component/";
    public static final String PREDICATE = "/predicate/";
    protected final Model rdfModel;
    protected final Property hasComponents;
    protected final Property isConnected;


    public  RDFDataFileCreator( Model rdfModel){
        Preconditions.checkNotNull(rdfModel);
        this.rdfModel = rdfModel;
        hasComponents = rdfModel.createProperty(URL_PREFIX + PREDICATE + "has_components");
        isConnected = rdfModel.createProperty(URL_PREFIX + PREDICATE + "is_connected");
    }

    public abstract void addGlycanToRDFDataFile(Glycan glycan);


    public OutputStream write(OutputStream stream, RDFFormat serialization){
        Preconditions.checkNotNull(stream);
        Preconditions.checkNotNull(serialization);
        RDFDataMgr.write(stream, rdfModel, serialization);
        return stream;
    }

}
