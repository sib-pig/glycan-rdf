package org.expasy.glycanrdf.rdf.query;

import com.google.common.base.Preconditions;
import org.expasy.glycanrdf.query.QueryGenerator;
import org.expasy.mzjava.glycomics.mol.*;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class VirtuosoQueryGeneratorExtend implements QueryGenerator {

    private static final String GLYCAN_PREFIX = "prefix glycan:  <http://mzjava.expasy.org/glycan/>";
    private static final String PREDICATE_PREFIX = "prefix predicate:  <http://mzjava.expasy.org/predicate/>";
    private static final String COMPONENT_PREFIX = "prefix component:  <http://mzjava.expasy.org/component/>";
    private static final String STRUCTCONN_PREFIX = "prefix structureConnection:  <http://mzjava.expasy.org/structureConnection/>";
    private static final String SELECT = "SELECT ";
    private static final String DISTINCT  = "DISTINCT ";
    private static final String WHERE  = "WHERE { ";
    private static final String structureConnection = "?structureConnection";
    private static final String component = "?component";
    private static final String lineSeparator = System.getProperty("line.separator");
    private static final String dotEndLine = " . ";
    private static final String HAS_COMPONENTS = " predicate:has_components ";
    private static final String IS_A = " predicate:is_a_";
    private static final String IS_CONNECTED = " predicate:is_connected ";
    private static final String IS_SUBSTITUTENT_LINKAGE = " predicate:is_SubstituentLinkage ";
    private static final String IS_GLYCOSIDIC_LINKAGE = " predicate:is_GlycosidicLinkage ";
    private static final String HAS_LINKED_CARBON = " predicate:has_linkedCarbon_";
    private static final String HAS_ANOMER_CARBON = " predicate:has_anomerCarbon_";
    private static final String HAS_ANOMER_CONNECTION = " predicate:has_anomerConnection_";


    @Override
    public String generateQueryString(Glycan glycan){

        StringBuilder query = new StringBuilder();
        queryPrefix(query);

        int componentNumber = 0;
        query.append(structureConnection).append(HAS_COMPONENTS).append(component).append(componentNumber).append(dotEndLine).append(lineSeparator);

        if(glycan.getMonosaccharideChildren(glycan.getRoot()).isEmpty()){
            query.append(component).append(componentNumber).append(IS_A).append(glycan.getRoot().getName()).append(" ").append(component).append(componentNumber).append(dotEndLine).append(lineSeparator);
            Set<Substituent> substituents = glycan.getSubstituentChildren(glycan.getRoot());
            for(Substituent substituent : substituents){
                query.append(component).append("0").append(IS_CONNECTED).append(component).append(componentNumber).append(dotEndLine).append(lineSeparator);
                query.append(component).append("0").append(IS_SUBSTITUTENT_LINKAGE).append(component).append(componentNumber).append(dotEndLine).append(lineSeparator);
                query.append(componentNumber).append(IS_A).append(substituent.getName()).append(" ").append(component).append(componentNumber).append(dotEndLine).append(lineSeparator);
                SubstituentLinkage substituentLinkage = glycan.getSubstituentLinkage(glycan.getRoot(),substituent).get();
                if(substituentLinkage.getLinkedCarbon().isPresent()){
                    query.append(component).append("0").append(HAS_LINKED_CARBON).append(substituentLinkage.getLinkedCarbon().get()).append(" ").append(component).append(componentNumber).append(dotEndLine).append(lineSeparator);
                }
                componentNumber++;
            }
        } else {
            QueryLinkageAcceptor acceptor = new QueryLinkageAcceptor(glycan, query);
            glycan.forEachLinkage(SaccharideGraph.Traversal.BFS, acceptor);
        }

        query.append("}").append(lineSeparator);

        return  query.toString();
    }

    private void queryPrefix(StringBuilder query){

        query.append(GLYCAN_PREFIX).append(lineSeparator).append(PREDICATE_PREFIX).append(lineSeparator).append(COMPONENT_PREFIX).append(lineSeparator).append(STRUCTCONN_PREFIX).append(lineSeparator);
        query.append(SELECT).append(DISTINCT).append(structureConnection).append(lineSeparator);
        query.append(WHERE).append(lineSeparator);

    }



    private static class QueryLinkageAcceptor implements LinkageAcceptor{

        private final Glycan glycan;
        private final StringBuilder query;
        private int componentNumber = 0;
        private final Map<Monosaccharide,Integer> monosaccharideIdentifierMap = new IdentityHashMap<Monosaccharide,Integer>();

        public QueryLinkageAcceptor(Glycan glycan, StringBuilder query) {
            this.glycan = glycan;
            this.query = query;

            monosaccharideIdentifierMap.put(glycan.getRoot(),0);
            query.append(component).append(monosaccharideIdentifierMap.get(glycan.getRoot())).append(IS_A).append(glycan.getRoot().getName()).append(" ").append(component).append(monosaccharideIdentifierMap.get(glycan.getRoot())).append(dotEndLine).append(lineSeparator);
            componentNumber++;

            for(Substituent substituent : glycan.getSubstituentChildren(glycan.getRoot())) {
                addSubstituentLinkage(glycan.getRoot(),substituent,glycan.getSubstituentLinkage(glycan.getRoot(),substituent).get(),componentNumber);
            }
        }


        @Override
        public void accept(Monosaccharide monosaccharide, Monosaccharide monosaccharide2, GlycosidicLinkage glycosidicLinkage) {

            Preconditions.checkNotNull(monosaccharide);
            Preconditions.checkNotNull(monosaccharide2);

            if(!monosaccharideIdentifierMap.containsKey(monosaccharide)) {
                monosaccharideIdentifierMap.put(monosaccharide, componentNumber);
                query.append(component).append(monosaccharideIdentifierMap.get(monosaccharide)).append(IS_A).append(monosaccharide.getName()).append(" ").append(component).append(monosaccharideIdentifierMap.get(monosaccharide)).append(dotEndLine).append(lineSeparator);
                componentNumber++;
            }

            if(!monosaccharideIdentifierMap.containsKey(monosaccharide2)) {
                monosaccharideIdentifierMap.put(monosaccharide2, componentNumber);
                query.append(component).append(monosaccharideIdentifierMap.get(monosaccharide2)).append(IS_A).append(monosaccharide2.getName()).append(" ").append(component).append(monosaccharideIdentifierMap.get(monosaccharide2)).append(dotEndLine).append(lineSeparator);
                componentNumber++;
            }

            addMonosaccharideLinkage(monosaccharide,monosaccharide2,glycosidicLinkage);
            for(Substituent substituent : glycan.getSubstituentChildren(monosaccharide2)) {
                addSubstituentLinkage(monosaccharide2,substituent,glycan.getSubstituentLinkage(monosaccharide2,substituent).get(),componentNumber);
            }
        }



        private void addMonosaccharideLinkage(Monosaccharide monosaccharide, Monosaccharide monosaccharide2, GlycosidicLinkage linkage){

            Preconditions.checkNotNull(linkage);
            query.append(component).append(monosaccharideIdentifierMap.get(monosaccharide)).append(IS_CONNECTED).append(component).append(monosaccharideIdentifierMap.get(monosaccharide2)).append(dotEndLine).append(lineSeparator);
            query.append(component).append(monosaccharideIdentifierMap.get(monosaccharide)).append(IS_GLYCOSIDIC_LINKAGE).append(component).append(monosaccharideIdentifierMap.get(monosaccharide2)).append(dotEndLine).append(lineSeparator);

            if(linkage.getAnomericCarbon().isPresent()){
                query.append(component).append(monosaccharideIdentifierMap.get(monosaccharide)).append(HAS_ANOMER_CONNECTION).append(linkage.getAnomericity().get()+ " ").append(component).append(monosaccharideIdentifierMap.get(monosaccharide2)).append(dotEndLine).append(lineSeparator);
            }

            if(linkage.getLinkedCarbon().isPresent()){
                query.append(component).append(monosaccharideIdentifierMap.get(monosaccharide)).append(HAS_LINKED_CARBON).append(linkage.getLinkedCarbon().get()+ " ").append(component).append(monosaccharideIdentifierMap.get(monosaccharide2)).append(dotEndLine).append(lineSeparator);
            }

            if(linkage.getAnomericCarbon().isPresent()){
                query.append(component).append(monosaccharideIdentifierMap.get(monosaccharide)).append(HAS_ANOMER_CARBON).append(linkage.getAnomericCarbon().get()+ " ").append(component).append(monosaccharideIdentifierMap.get(monosaccharide2)).append(dotEndLine).append(lineSeparator);
            }
        }


        private void addSubstituentLinkage(Monosaccharide monosaccharide, Substituent substituent, SubstituentLinkage linkage, int substituentNumber){

            Preconditions.checkNotNull(linkage);
            query.append(component).append(componentNumber).append(IS_A).append(substituent.getName()).append(" ").append(component).append(componentNumber).append(dotEndLine).append(lineSeparator);
            query.append(component).append(monosaccharideIdentifierMap.get(monosaccharide)).append(IS_CONNECTED).append(component).append(substituentNumber).append(dotEndLine).append(lineSeparator);
            query.append(component).append(monosaccharideIdentifierMap.get(monosaccharide)).append(IS_SUBSTITUTENT_LINKAGE).append(component).append(substituentNumber).append(dotEndLine).append(lineSeparator);

            if(linkage.getLinkedCarbon().isPresent()){
                query.append(component).append(monosaccharideIdentifierMap.get(monosaccharide)).append(HAS_LINKED_CARBON).append(linkage.getLinkedCarbon().get()+ " ").append(component).append(substituentNumber).append(dotEndLine).append(lineSeparator);
            }
            componentNumber++;
        }



    }

}
