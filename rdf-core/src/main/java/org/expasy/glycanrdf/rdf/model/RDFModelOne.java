package org.expasy.glycanrdf.rdf.model;

import com.google.common.base.Preconditions;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import org.expasy.mzjava.glycomics.io.mol.glycoct.GlycoCTReader;
import org.expasy.mzjava.glycomics.mol.*;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 * This is the first implementation of the RDF datafile creator.
 * In this case we have:
 * 1) glycan -> has_structure
 * 2) structureConnection -> has_components
 * 3) components -> isConnected
 *               -> is_monosaccharide / is_substituent
 *               -> is_a.....(gal,glc..)
 *               -> is_GlycosidicLinkage
 *               -> is_SubstituentLinkage
 *               -> has_anomerConnectionBeta
 *               -> has_anomerConnectionAlpha
 *               -> has_anomerCarbon1....6
 *               -> has_linkedCarbon1....6
 * Known problem :
 * 1) Anomericy of the End. Is it necessary to add the anomericity of the end (root anomericity)?
 *
 */
public class RDFModelOne extends RDFDataFileCreator {

    private final Property hasStructure;
    private final Property isMonosaccharide;
    private final Property isSubstituent;
    private final Property isGlycosdicLinakge;
    private final Property isSubstituentLinkage;
    private static final String IS_A = "is_a_";
    private static final String HAS_LINKED_CARBON = "has_linkedCarbon_";

    public RDFModelOne(Model model) {

        super(model);
        hasStructure = rdfModel.createProperty(URL_PREFIX + PREDICATE + "has_structure");
        isMonosaccharide = rdfModel.createProperty(URL_PREFIX + PREDICATE + "is_monosaccharide");
        isSubstituent = rdfModel.createProperty(URL_PREFIX + PREDICATE + "is_substituent");
        isGlycosdicLinakge = rdfModel.createProperty(URL_PREFIX + PREDICATE + "is_GlycosidicLinkage");
        isSubstituentLinkage = rdfModel.createProperty(URL_PREFIX + PREDICATE + "is_SubstituentLinkage");

    }


    public void addGlycoCTToRDFDataFile(String glycoCTString, String databaseIdentifier){

        Preconditions.checkNotNull(glycoCTString);
        Preconditions.checkNotNull(databaseIdentifier);
        if(glycoCTString.isEmpty()){
            throw new IllegalArgumentException("Cannot import empty string.");
        }

        GlycoCTReader reader = new GlycoCTReader();
        addGlycanToRDFDataFile(reader.read(glycoCTString, databaseIdentifier));
    }

    @Override
    public void addGlycanToRDFDataFile(Glycan glycan) {

        Preconditions.checkNotNull(glycan);
        Resource rdfGlycan = rdfModel.createResource(URL_PREFIX + "/glycan/" + glycan.getDatabaseIdentifier()  );
        rdfGlycan.addProperty(hasStructure,createStructure(glycan));

    }


    private Resource createStructure(Glycan glycan){

        Resource rdfStructure = rdfModel.createResource(URL_PREFIX + "/structureConnection/" + glycan.getDatabaseIdentifier() );

        if(glycan.getMonosaccharideChildren(glycan.getRoot()).isEmpty()){
            createStructureOnlyRoot(glycan,rdfStructure);
        } else {
            LinkageAcceptor linkageAcceptor = new RDFLinkageAcceptor(glycan, rdfStructure);
            glycan.forEachLinkage(SaccharideGraph.Traversal.BFS, linkageAcceptor);
        }

        return rdfStructure;

    }


    private void createStructureOnlyRoot(Glycan glycan, Resource rdfStructure){

        Resource monosaccharideRes = rdfModel.createResource(URL_PREFIX + COMPONENT + glycan.getDatabaseIdentifier() + "/0");
        rdfStructure.addProperty(hasComponents,monosaccharideRes);
        monosaccharideRes.addProperty(isMonosaccharide,monosaccharideRes);
        monosaccharideRes.addProperty(rdfModel.createProperty(URL_PREFIX + PREDICATE + IS_A + glycan.getRoot().getName()),monosaccharideRes);

        int countSubstituent = 1;
        for(Substituent substituent : glycan.getSubstituentChildren(glycan.getRoot())){
            Resource substituentRes = rdfModel.createResource(URL_PREFIX + COMPONENT + glycan.getDatabaseIdentifier() + "/" + countSubstituent);
            rdfStructure.addProperty(hasComponents,substituentRes);
            substituentRes.addProperty(isSubstituent,substituentRes);
            substituentRes.addProperty(rdfModel.createProperty(URL_PREFIX + PREDICATE + IS_A +substituent.getName()),substituentRes);
            if(glycan.getSubstituentLinkage(glycan.getRoot(),substituent).isPresent()){
                SubstituentLinkage linkage = glycan.getSubstituentLinkage(glycan.getRoot(),substituent).get();
                monosaccharideRes.addProperty(isConnected,substituentRes);
                monosaccharideRes.addProperty(isSubstituentLinkage,substituentRes);
                if(linkage.getLinkedCarbon().isPresent()){
                    monosaccharideRes.addProperty(rdfModel.createProperty(URL_PREFIX + PREDICATE + HAS_LINKED_CARBON + linkage.getLinkedCarbon().get()),substituentRes);
                }
            }
        }
    }


    private class RDFLinkageAcceptor implements LinkageAcceptor {

        private final Map<Monosaccharide,Resource> monosaccharideResourceMap;
        private final Map<Substituent,Resource> substituentResourceMap;
        private final Glycan glycan;
        private final Resource rdfStructure;

        public RDFLinkageAcceptor(Glycan glycan, Resource rdfStructure) {
            this.glycan = glycan;
            this.rdfStructure = rdfStructure;
            monosaccharideResourceMap = new IdentityHashMap<Monosaccharide, Resource>();
            substituentResourceMap = new IdentityHashMap<Substituent, Resource>();
        }

        @Override
        public void accept(Monosaccharide monosaccharide, Monosaccharide monosaccharide2, GlycosidicLinkage glycosidicLinkage) {

            addComponentWithSubstituent(monosaccharide);
            addComponentWithSubstituent(monosaccharide2);

            monosaccharideResourceMap.get(monosaccharide).addProperty(isConnected,monosaccharideResourceMap.get(monosaccharide2));
            monosaccharideResourceMap.get(monosaccharide).addProperty(isGlycosdicLinakge,monosaccharideResourceMap.get(monosaccharide2));

            if(glycosidicLinkage.getAnomericity().isPresent()){
                monosaccharideResourceMap.get(monosaccharide).addProperty(rdfModel.createProperty(URL_PREFIX + PREDICATE + "has_anomerConnection_" + glycosidicLinkage.getAnomericity().get()),monosaccharideResourceMap.get(monosaccharide2));
            }
            if(glycosidicLinkage.getAnomericCarbon().isPresent()){
                monosaccharideResourceMap.get(monosaccharide).addProperty(rdfModel.createProperty(URL_PREFIX + PREDICATE + "has_anomerCarbon_" + glycosidicLinkage.getAnomericCarbon().get()),monosaccharideResourceMap.get(monosaccharide2));
            }
            if(glycosidicLinkage.getLinkedCarbon().isPresent()){
                monosaccharideResourceMap.get(monosaccharide).addProperty(rdfModel.createProperty(URL_PREFIX + PREDICATE + HAS_LINKED_CARBON + glycosidicLinkage.getLinkedCarbon().get()),monosaccharideResourceMap.get(monosaccharide2));
            }
        }


        private void addComponentWithSubstituent(Monosaccharide monosaccharide){

            Preconditions.checkNotNull(monosaccharide);
            if(monosaccharideResourceMap.containsKey(monosaccharide)){
                return;
            }

            Resource monosaccharideRes = rdfModel.createResource(URL_PREFIX + COMPONENT + glycan.getDatabaseIdentifier() + "/" + (monosaccharideResourceMap.size() + substituentResourceMap.size()));
            rdfStructure.addProperty(hasComponents,monosaccharideRes);
            monosaccharideRes.addProperty(isMonosaccharide,monosaccharideRes);
            monosaccharideRes.addProperty(rdfModel.createProperty(URL_PREFIX + PREDICATE + IS_A + monosaccharide.getName()),monosaccharideRes);
            monosaccharideResourceMap.put(monosaccharide,monosaccharideRes);

            NodeSet<Substituent> substituents = glycan.getSubstituentChildren(monosaccharide);

            for(Substituent substituent : substituents){
                Resource substituentRes = rdfModel.createResource(URL_PREFIX + COMPONENT + glycan.getDatabaseIdentifier() + "/" + (monosaccharideResourceMap.size() + substituentResourceMap.size()));
                rdfStructure.addProperty(hasComponents,substituentRes);
                substituentRes.addProperty(isSubstituent,substituentRes);
                substituentRes.addProperty(rdfModel.createProperty(URL_PREFIX + PREDICATE + IS_A +substituent.getName()),substituentRes);
                substituentResourceMap.put(substituent,substituentRes);
                if(glycan.getSubstituentLinkage(monosaccharide,substituent).isPresent()){
                    SubstituentLinkage linkage = glycan.getSubstituentLinkage(monosaccharide,substituent).get();
                    monosaccharideRes.addProperty(isConnected,substituentRes);
                    monosaccharideRes.addProperty(isSubstituentLinkage,substituentRes);
                    if(linkage.getLinkedCarbon().isPresent()){
                        monosaccharideRes.addProperty(rdfModel.createProperty(URL_PREFIX + PREDICATE + HAS_LINKED_CARBON + linkage.getLinkedCarbon().get()),substituentRes);
                    }
                }
            }
        }
    }
}
