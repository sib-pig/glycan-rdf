package org.expasy.glycanrdf.rdf.database;

import org.apache.log4j.Logger;
import org.expasy.glycanrdf.configuration.ConfigurationManager;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import virtuoso.sesame2.driver.VirtuosoRepository;


/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

@ServiceProvider(service = DatabaseDriver.class)
public class VirtuosoDriver implements DatabaseDriver {

    private static final Logger LOGGER = Logger.getLogger(DatabaseConnector.class);
    private final ConfigurationManager manager = Lookup.getDefault().lookup(ConfigurationManager.class);
    private Repository virtuosoRepository = null;


    @Override
    public RepositoryConnection getConnection() {
        if(virtuosoRepository == null){
            throw new IllegalStateException("Cannot get connection from Virtuoso database. Please initialize the repository before using it.");
        }
        try{
            return virtuosoRepository.getConnection();
        } catch (RepositoryException e) {
            throw new IllegalStateException("Cannot get connection from Virtuoso database. Please check the configuration file.",e);
        }
    }

    @Override
    public String getDriverType() {
        return "virtuoso";
    }

    @Override
    public void initialize() {
        try {
            String url = manager.getProperty("virtuoso.url");
            String port = manager.getProperty("virtuoso.port");
            String user = manager.getProperty("virtuoso.user");
            String password = manager.getProperty("virtuoso.password");
            String database = manager.getProperty("virtuoso.dbname");

            virtuosoRepository = new VirtuosoRepository("jdbc:virtuoso://"+url+":"+port+"/"+database, user, password);
            virtuosoRepository.initialize();
            LOGGER.info("Virtuoso connection ready to use.");

        } catch (RepositoryException e) {
            String errorMessage = "Cannot connect to Virtuoso database. Please check the configuration file";
            LOGGER.fatal(errorMessage);
            throw new IllegalStateException(errorMessage,e);
        }
    }

}
