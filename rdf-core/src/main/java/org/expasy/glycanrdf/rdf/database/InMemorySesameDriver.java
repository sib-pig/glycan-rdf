package org.expasy.glycanrdf.rdf.database;

import org.apache.log4j.Logger;
import org.expasy.glycanrdf.configuration.ConfigurationManager;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.sail.memory.MemoryStore;

import java.io.File;
import java.io.IOException;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

@ServiceProvider(service = DatabaseDriver.class)
public class InMemorySesameDriver implements DatabaseDriver {

    private static final Logger LOGGER = Logger.getLogger(DatabaseConnector.class);

    private Repository inMemoryRepository = null;
    ConfigurationManager manager = Lookup.getDefault().lookup(ConfigurationManager.class);

    @Override
    public RepositoryConnection getConnection() {
        if(inMemoryRepository == null){
            throw new IllegalStateException("Cannot get connection from in memory repository. Please initialize the repository before using it.");
        }
        try{
            return inMemoryRepository.getConnection();
        } catch (RepositoryException e) {
            throw new IllegalStateException("Cannot get connection from in memory repository.",e);
        }
    }

    @Override
    public String getDriverType() {
        return "sesameInMemory";
    }

    @Override
    public void initialize() {
        inMemoryRepository = new SailRepository(new MemoryStore());
        try {
            inMemoryRepository.initialize();
            String file = manager.getProperty("sesameInMemory.file");
            String encoding = manager.getProperty("sesameInMemory.encoding");
            String url = manager.getProperty("sesameInMemory.url");
            inMemoryRepository.getConnection().add(new File(file),url, getEncoding(encoding));

        } catch (RepositoryException e) {
            String errorMessage = "Cannot Create in memory database. Please check the configuration file";
            LOGGER.fatal(errorMessage);
            throw new IllegalStateException(errorMessage,e);
        } catch (RDFParseException e) {
            String errorMessage = "Error parsing the file specified in the configuration file. Refer to the encoding define in package org.openrdf.rio.RDFFormat";
            LOGGER.fatal(errorMessage);
            throw new IllegalStateException(errorMessage,e);
        } catch (IOException e) {
            String errorMessage = "Error loading the file specified in the configuration file. Please have a look at it";
            LOGGER.fatal(errorMessage);
            throw new IllegalStateException(errorMessage,e);
        }
    }



    private RDFFormat getEncoding(String encoding){
        switch (encoding) {
            case "Turtle":
                return RDFFormat.TURTLE;
            case "RDF/XML":
                return RDFFormat.RDFXML;
            case "N-Triples":
                return RDFFormat.NTRIPLES;
            case "N3":
                return RDFFormat.N3;
            case "BinaryRDF":
                return RDFFormat.BINARY;
            case "JSON-LD":
                return RDFFormat.JSONLD;
            case "N-Quads":
                return RDFFormat.NQUADS;
            case "RDF/JSON":
                return RDFFormat.RDFJSON;
            default:
                throw new IllegalStateException("Wrong Encoding. Please check the configuration file. " +
                        "Accepted value Turtle, RDF/XML, N3, BinaryRDF, JSON-LD, N-Quads, RDF/JSON");
        }
    }
}
