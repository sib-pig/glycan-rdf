package org.expasy.glycanrdf.rdf.database;

import org.apache.log4j.Logger;
import org.expasy.glycanrdf.configuration.ConfigurationManager;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;


/**
 * Created by dalocci on 10/9/15.
 */
@ServiceProvider(service = DatabaseDriver.class)
public class FusekiDriver implements DatabaseDriver {

    private static final Logger LOGGER = Logger.getLogger(DatabaseConnector.class);
    private ConfigurationManager manager = Lookup.getDefault().lookup(ConfigurationManager.class);
    private Repository fusekiRepository = null;

    @Override
    public RepositoryConnection getConnection() {
        if(fusekiRepository == null){
            throw new IllegalStateException("Cannot get connection from Fuseki Jena database. Please initialize the repository before using it.");
        }
        try{
            return fusekiRepository.getConnection();
        } catch (RepositoryException e) {
            throw new IllegalStateException("Cannot get connection from Fuseki Jena database. Please check the configuration file.",e);
        }
    }

    @Override
    public String getDriverType() {
        return "jena";
    }

    @Override
    public void initialize() {

        try {
            String url = manager.getProperty("jena.url");
            String port = manager.getProperty("jena.port");
            String database = manager.getProperty("jena.dbname");

            fusekiRepository = new HTTPRepository("http://"+url+":"+port+"/"+database+"/query");
            fusekiRepository.initialize();

            LOGGER.info("Fuseki Jena connection ready to use.");

        } catch (RepositoryException e) {
            String errorMessage = "Cannot connect to Fuseki Jena database. Please check the configuration file";
            LOGGER.fatal(errorMessage);
            throw new IllegalStateException(errorMessage,e);
        }
    }
}
