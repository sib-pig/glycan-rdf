<!DOCTYPE html>
<jsp:include page="template/head.jsp"></jsp:include>
<body class="size-1140">
<!-- TOP NAV WITH LOGO -->

<jsp:include page="template/header.jsp"></jsp:include>

<!-- ASIDE NAV AND CONTENT -->
<div class="line">
    <div class="box  margin-bottom">
        <div class="margin">
            <!-- ASIDE NAV 2 -->
            <jsp:include page="template/menu.jsp"></jsp:include>
            <!-- CONTENT -->
            <section class="s-12 l-6">
                <h1>Substructure Search</h1>
                <p>Something wrong happens. Please look in the logs of the server.</p>
            </section>
        </div>
    </div>
</div>
<!-- FOOTER -->
<jsp:include page="template/footer.jsp"></jsp:include>
</body>
</html>