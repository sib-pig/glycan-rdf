<aside class="s-12 l-3">
    <h3>Menu</h3>
    <div class="aside-nav">
        <ul>
            <li><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
            <li><a href="${pageContext.request.contextPath}/contact.jsp">Contact</a></li>
        </ul>
    </div>
</aside>