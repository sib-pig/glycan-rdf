<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=windows-1250">
    <meta name="viewport" content="width=device-width" />
    <title>Substructure Search</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/components.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/responsee.css">
    <!-- CUSTOM STYLE -->
    <link rel="stylesheet" href="css/template-style.css">

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.7.0/jquery-ui.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/modernizr.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/responsee.js"></script>

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>