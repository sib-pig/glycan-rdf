<header>
    <nav>
        <div class="line">
            <div class="s-12 l-5">
                <img class="s-5 l-12 center" src="${pageContext.request.contextPath}/img/sib_logo_141x75.gif">
            </div>
            <div class="top-nav s-12 l-7 right">
                <p class="nav-text">Custom menu text</p>
                <ul class="right">
                    <li><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                    <li><a href="${pageContext.request.contextPath}/contact.jsp">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>