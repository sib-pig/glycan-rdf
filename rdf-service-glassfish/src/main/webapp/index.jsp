<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<jsp:include page="template/head.jsp"></jsp:include>
<body class="size-1140">
  <!-- TOP NAV WITH LOGO -->
  <jsp:include page="template/header.jsp"></jsp:include>

  <!-- ASIDE NAV AND CONTENT -->
  <div class="line">
	<div class="box  margin-bottom">
	  <div class="margin">
		<!-- ASIDE NAV 2 -->
          <jsp:include page="template/menu.jsp"></jsp:include>

          <!-- CONTENT -->
		<section class="s-12 l-8">
		  <h1>Substructure Search</h1>
            <form method="post" action="service/substructureSearch">
                <br />
                <p>Insert the glycan substructure in GlycoCt condensed format:</p>
                <br /><br />
                <textarea name="GlycoCTCode" id="GlycoCTCode" rows="5" cols="40">Insert here</textarea>
                <br />
                <input type="submit" value="Submit" />
            </form>
            <br />
            <br />
            <h3>Substructure Example GlycoCt condensed format:</h3>
            <br />
            <p> RES</p>
            <p> 1b:x-dglc-HEX-1:5</p>
            <p> 2s:n-acetyl</p>
            <p> 3b:b-dgal-HEX-1:5</p>
            <p> 4b:b-dglc-HEX-1:5</p>
            <p> 5s:n-acetyl</p>
            <p> LIN</p>
            <p> 1:1d(2+1)2n</p>
            <p> 2:1o(3+1)3d</p>
            <p> 3:1o(6+1)4d</p>
            <p> 4:4d(2+1)5n</p>
		</section>
	  </div>
	</div>  
  </div>
  <!-- FOOTER -->
  <jsp:include page="template/footer.jsp"></jsp:include>
</body>
</html>