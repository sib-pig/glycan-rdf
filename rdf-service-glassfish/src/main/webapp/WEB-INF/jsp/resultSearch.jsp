<%--
  @author Davide Alocci
  @version sqrt -1.
--%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Substructure Search Results</title>
</head>
<body>

<h2>The query has produced ${it.counter} results on 17000 structures in the Database.</h2>
<table>
    <c:forEach var="id" items="${it.ids}">
        <tr>
            <td> Glycome Db ID of the Sugar ${id}</td>
            <td> <a href="http://www.glycome-db.org/database/showStructure.action?glycomeId=${id}"><img src="http://www.glycome-db.org/getSugarImage.action?id=${id}&type=cfg"></a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
