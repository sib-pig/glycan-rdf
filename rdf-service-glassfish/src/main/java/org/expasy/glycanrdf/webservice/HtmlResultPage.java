package org.expasy.glycanrdf.webservice;


import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.eurocarbdb.application.glycanbuilder.*;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public class HtmlResultPage {

    private final StringBuilder stringBuilder = new StringBuilder();
    private static final Logger LOGGER = Logger.getLogger(SubstructureSearchResource.class);



    public void addHeader(){

        String context = "/rdf-service-glassfish";

        String header = "<!DOCTYPE html>\n" +
                "<html xmlns=\"http://www.w3.org/1999/html\">\n" +
                "<head>\n" +
                "    <meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1250\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width\" />\n" +
                "    <title>Substructure Search</title>\n" +
                "    <link rel=\"stylesheet\" href=\""+context+"/css/components.css\">\n" +
                "    <link rel=\"stylesheet\" href=\""+context+"/css/responsee.css\">\n" +
                "    <!-- CUSTOM STYLE -->\n" +
                "    <link rel=\"stylesheet\" href=\"css/template-style.css\">\n" +
                "\n" +
                "    <script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-1.8.3.min.js\"></script>\n" +
                "    <script type=\"text/javascript\" src=\"http://code.jquery.com/ui/1.7.0/jquery-ui.min.js\"></script>\n" +
                "    <script type=\"text/javascript\" src=\""+context+"/js/modernizr.js\"></script>\n" +
                "    <script type=\"text/javascript\" src=\""+context+"/js/responsee.js\"></script>\n" +
                "\n" +
                "    <!--[if lt IE 9]>\n" +
                "    <script src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script>\n" +
                "    <![endif]-->\n" +
                "</head>\n"+
                "<body class=\"size-1140\">\n" +
                "<header>\n" +
                "    <nav>\n" +
                "        <div class=\"line\">\n" +
                "            <div class=\"s-12 l-5\">\n" +
                "                <img class=\"s-5 l-12 center\" src=\""+context+"/img/sib_logo_141x75.gif\">\n" +
                "            </div>\n" +
                "            <div class=\"top-nav s-12 l-7 right\">\n" +
                "                <p class=\"nav-text\">Custom menu text</p>\n" +
                "                <ul class=\"right\">\n" +
                "                    <li><a href=\""+context+"/index.jsp\">Home</a></li>\n" +
                "                    <li><a href=\""+context+"/contact.jsp\">Contact</a></li>\n" +
                "                </ul>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </nav>\n" +
                "</header>\n"+
                "  <!-- ASIDE NAV AND CONTENT -->\n" +
                "  <div class=\"line\">\n" +
                "\t<div class=\"box  margin-bottom\">\n" +
                "\t  <div class=\"margin\">\n" +
                "\t\t<!-- ASIDE NAV 2 -->\n" +
                "<aside class=\"s-12 l-3\">\n" +
                "    <h3>Menu</h3>\n" +
                "    <div class=\"aside-nav\">\n" +
                "        <ul>\n" +
                "                    <li><a href=\""+context+"/index.jsp\">Home</a></li>\n" +
                "                    <li><a href=\""+context+"/contact.jsp\">Contact</a></li>\n" +
                "        </ul>\n" +
                "    </div>\n" +
                "</aside>\n"+
                "\n";
        stringBuilder.append(header);
    }


    public void addContent(int size,String glycanCT){

        String content ="          <!-- CONTENT -->\n" +
                "\t\t<section class=\"s-12 l-8\">\n"+
                "<br />\n" +
                "<h2>The query has produced "+size+" results on 17000 structures in the Database.</h2>"+
                "</br>"+
                "<h3>Search</h3>" +
                "<table style=\"width:100%\">"+
                "<tr>\n" +
                "    <td> Query Structure </td>\n" +
                "    <td><img src=\"data:image/jpg;base64,"+generateQueryImage(glycanCT)+"\"></td>"+
                "</tr>"+
                "</table>"+

                "</br>"+
                "<h3>Query results:</h3>" +
                "<table style=\"width:100%\">";
        stringBuilder.append(content);
    }


    public void addResult(String id){
        String result = "<tr>\n" +
                        "    <td> Glycome Db ID : " + id +"</td>\n" +
                        "    <td> <a href=\"http://www.glycome-db.org/database/showStructure.action?glycomeId="+id+"\"><img src=\"http://www.glycome-db.org/getSugarImage.action?id="+id+"&type=cfg\"></a></td>"+
                        "</tr>";
        stringBuilder.append(result);
    }

    public void addFooter(){
        String footer = "</table>\n" +
                "\t\t</section>\n" +
                "\t  </div>\n" +
                "\t</div>  \n" +
                "  </div>\n" +
                "  <!-- FOOTER -->\n" +
                "<footer class=\"box\">\n" +
                "    <div class=\"line\">\n" +
                "        <div class=\"s-12 l-6\">\n" +
                "            <p>&#64; 2014 Swiss Institute of Bioinformatics, All Rights Reserved</p>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</footer>\n"+
                        "</body>\n" +
                        "</html>";

        stringBuilder.append(footer);
    }

    private String generateQueryImage(String querySugar){

        String graphicFormat = "jpg"; //png | jpg |svg

        BuilderWorkspace workspace=new BuilderWorkspace(new GlycanRendererAWT());
        workspace.setNotation("cfg"); //cfg | cfgbw | uoxf | uoxfcol | text

        //Get a reference to the renderer
        GlycanRenderer renderer=workspace.getGlycanRenderer();

        //Get an instance of the GlycoCT Parser
        GlycoCTCondensedParser parser=new GlycoCTCondensedParser(false);
        MassOptions options= MassOptions.empty();
        try
        {
            //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
            Glycan glycan = parser.fromGlycoCTCondensed(querySugar, options);

            //Generate a list of glycans to render
            List<Glycan> glycanList=new ArrayList<Glycan>();
            glycanList.add(glycan);


            //Render glycans as a PNG or SVG
            return Base64.encodeBase64String(SVGUtils.export((GlycanRendererAWT) renderer,  glycanList, false, true, graphicFormat));
        } catch(Exception e) {
            String errorMessage ="Cannot create picture.";
            LOGGER.fatal(errorMessage);
            throw new IllegalStateException(errorMessage, e);
        }
    }

    public String build(){
        return stringBuilder.toString();
    }


}
