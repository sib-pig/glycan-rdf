package org.expasy.glycanrdf.webservice;


import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.expasy.glycanrdf.query.QueryGenerator;
import org.expasy.glycanrdf.query.QueryLauncher;
import org.expasy.glycanrdf.rdf.query.VirtuosoQueryGeneratorExtend;
import org.expasy.mzjava.glycomics.io.mol.glycoct.GlycoCTReader;
import org.expasy.mzjava.glycomics.mol.Glycan;
import org.openrdf.query.TupleQueryResultHandler;
import org.openrdf.query.resultio.sparqljson.SPARQLResultsJSONWriter;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.UUID;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

@Path("/substructureSearch")
public class SubstructureSearchResource {

    private static final Logger LOGGER = Logger.getLogger(SubstructureSearchResource.class);

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("text/html")
    public Response postIt(@FormParam("GlycoCTCode") String glycoCtCode)  {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GlycoCTReader reader = new GlycoCTReader();
        Glycan glycan = reader.read(glycoCtCode,"QuerySugar");

        LOGGER.info("Query started.");

        TupleQueryResultHandler resultHandler = new SPARQLResultsJSONWriter(out);
        QueryGenerator queryGenerator = new VirtuosoQueryGeneratorExtend();
        QueryLauncher queryLauncher = new QueryLauncher();
        queryLauncher.queryGlycanSPARQL(glycan,resultHandler, queryGenerator);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode resultNode;
        try {
             resultNode = mapper.readValue(out.toString("UTF8"), JsonNode.class);
        } catch (IOException e) {
            String errorMessage = "Cannot read results value from SPARQL Query";
            LOGGER.fatal(errorMessage);
            throw new IllegalStateException(errorMessage,e);
        }

        LOGGER.info("Query executed.");

        HtmlResultPage htmlResultPage = new HtmlResultPage();
        htmlResultPage.addHeader();
        htmlResultPage.addContent(resultNode.path("results").path("bindings").size(),glycoCtCode);

        for (JsonNode binding : resultNode.path("results").path("bindings")) {
            String glycomeID = binding.path("structureConnection").path("value").getTextValue().replaceAll("[^\\d.]", "").replace(".","");
            htmlResultPage.addResult(glycomeID);
        }

        htmlResultPage.addFooter();
        return Response.created(URI.create("/substructureSearch/" + UUID.randomUUID())).type(MediaType.TEXT_HTML).entity(htmlResultPage.build()).build();
    }


}
